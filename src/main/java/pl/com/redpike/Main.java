package pl.com.redpike;

import com.alee.laf.WebLookAndFeel;
import pl.com.redpike.presentation.login.LoginFrame;

import javax.swing.*;

/**
 * Created by Redpike.
 */
public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            WebLookAndFeel.install();
            LoginFrame loginFrame = new LoginFrame();
            loginFrame.init();
        });
    }
}
