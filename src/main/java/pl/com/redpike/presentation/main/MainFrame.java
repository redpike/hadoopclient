package pl.com.redpike.presentation.main;

import com.alee.laf.button.WebButton;
import com.alee.managers.notification.NotificationManager;
import pl.com.redpike.business.uzytkownik.Uzytkownik;
import pl.com.redpike.control.login.UserAuthorization;
import pl.com.redpike.presentation.credits.CreditsPanel;
import pl.com.redpike.presentation.komisariat.KomisariatPanel;
import pl.com.redpike.presentation.login.LoginFrame;
import pl.com.redpike.presentation.petent.PetentPanel;
import pl.com.redpike.presentation.pracownik.PracownikPanel;
import pl.com.redpike.presentation.zdarzenie.ZdarzeniePanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Redpike.
 */
public class MainFrame extends JFrame {

    private JPanel masterPanel;
    private JPanel mainMenuPanel;
    private JPanel contentPanel;
    private JPanel authorsPanel;
    private GridBagConstraints gbc;

    private KomisariatPanel komisariatPanel;
    private PracownikPanel pracownikPanel;
    private ZdarzeniePanel zdarzeniePanel;
    private PetentPanel petentPanel;
    private CreditsPanel creditsPanel;

    private BufferedImage komisariatIcon;
    private BufferedImage pracownikIcon;
    private BufferedImage zdarzenieIcon;
    private BufferedImage petentIcon;
    private BufferedImage creditsIcon;
    private BufferedImage logoutIcon;

    private WebButton komisariatButton;
    private WebButton pracownikButton;
    private WebButton zdarzenieButton;
    private WebButton petentButton;
    private WebButton creditsButton;
    private WebButton logoutButton;

    private Uzytkownik uzytkownik;

    public MainFrame() {

    }

    public MainFrame(Uzytkownik uzytkownik) {
        this.uzytkownik = uzytkownik;
    }

    public void init() {
        prepareWindow();
        loadIcons();
        initComponents();
        initLayout();
        initListeners();
        setVisible(true);
    }

    private void prepareWindow() {
        setSize(800, 600);
        centerWindow();
        setTitle("Kronika policyjna - Hadoop Project");
        Image image = Toolkit.getDefaultToolkit()
                .createImage("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\logoSmall.png");
        setIconImage(image);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initComponents() {
        masterPanel = new JPanel(new BorderLayout(10, 10));

        mainMenuPanel = new JPanel(new GridBagLayout());
        mainMenuPanel.setPreferredSize(new Dimension(100, getContentPane().getHeight()));
        mainMenuPanel.setBackground(new Color(63, 179, 94));

        authorsPanel = new JPanel();
        authorsPanel.add(new JLabel("Sokulski Rafał, Tomaszewski Piotr, Zabawa Adrian"));

        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.VERTICAL;

        komisariatButton = new WebButton("Komisariaty", new ImageIcon(komisariatIcon));
        komisariatButton.setPreferredSize(100, 40);

        pracownikButton = new WebButton("Pracownicy", new ImageIcon(pracownikIcon));
        pracownikButton.setPreferredSize(100, 40);

        zdarzenieButton = new WebButton("Zdarzenia", new ImageIcon(zdarzenieIcon));
        zdarzenieButton.setPreferredSize(100, 40);

        petentButton = new WebButton("Petenci", new ImageIcon(petentIcon));
        petentButton.setPreferredSize(100, 40);

        creditsButton = new WebButton("Credits", new ImageIcon(creditsIcon));
        creditsButton.setPreferredSize(100, 40);

        logoutButton = new WebButton("Logout", new ImageIcon(logoutIcon));
        logoutButton.setPreferredSize(100, 40);

        creditsPanel = new CreditsPanel();

        contentPanel = new JPanel();
        contentPanel.setBackground(new Color(130, 210, 151));
        contentPanel.add(creditsPanel);
    }

    private void initLayout() {
        gbc.gridx = 0;
        gbc.gridy = 0;
        mainMenuPanel.add(komisariatButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(5, 0, 0, 0);
        mainMenuPanel.add(pracownikButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(5, 0, 0, 0);
        mainMenuPanel.add(zdarzenieButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.insets = new Insets(5, 0, 0, 0);
        mainMenuPanel.add(petentButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.insets = new Insets(5, 0, 0, 0);
        mainMenuPanel.add(creditsButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.insets = new Insets(265, 0, 0, 0);
        mainMenuPanel.add(logoutButton, gbc);

        masterPanel.add(mainMenuPanel, BorderLayout.WEST);
        masterPanel.add(contentPanel, BorderLayout.CENTER);
        masterPanel.add(authorsPanel, BorderLayout.SOUTH);
        getContentPane().add(masterPanel);
    }

    private void initListeners() {
        komisariatButton.addActionListener(e -> {
            komisariatPanel = new KomisariatPanel();
            setNewContent(komisariatPanel);
        });

        pracownikButton.addActionListener(e -> {
            pracownikPanel = new PracownikPanel();
            setNewContent(pracownikPanel);
        });

        zdarzenieButton.addActionListener(e -> {
            zdarzeniePanel = new ZdarzeniePanel();
            setNewContent(zdarzeniePanel);
        });

        petentButton.addActionListener(e -> {
            petentPanel = new PetentPanel();
            setNewContent(petentPanel);
        });

        creditsButton.addActionListener(e -> {
            setNewContent(creditsPanel);
        });

        logoutButton.addActionListener(e -> {
            if (UserAuthorization.isLogged()) {
                NotificationManager.showNotification("User " + getUzytkownik().getUsername() + " has been logged out!");
                dispose();
                new LoginFrame().init();
            }
        });
    }

    private void centerWindow() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - getHeight()) / 2);
        setLocation(x, y);
    }

    private void setNewContent(JPanel content) {
        contentPanel.removeAll();
        contentPanel.repaint();
        contentPanel.revalidate();

        contentPanel.add(content);
        contentPanel.repaint();
        contentPanel.revalidate();
    }

    private void loadIcons() {
        try {
            komisariatIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\Komisariat.png"));

            pracownikIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\Pracownik.png"));

            zdarzenieIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\Zdarzenie.png"));

            petentIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\Petenci.png"));

            creditsIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\Petenci.png"));

            logoutIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\logout.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Uzytkownik getUzytkownik() {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik) {
        this.uzytkownik = uzytkownik;
    }
}
