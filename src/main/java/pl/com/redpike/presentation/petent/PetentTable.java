package pl.com.redpike.presentation.petent;

import pl.com.redpike.business.petent.Petent;
import pl.com.redpike.control.petent.PetentControl;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by Redpike.
 */
public class PetentTable extends AbstractTableModel {

    private PetentControl petentControl = new PetentControl();
    private List<Petent> petentList;

    private String[] columnNames = {
            "ID",
            "Name",
            "Surname",
            "Address",
            "Postal code",
            "Phone Number",
            "Email address",
            "Sex",
            "Notes"
    };

    public PetentTable() {
        petentList = petentControl.getAllPetentFromDB();
    }


    @Override
    public int getRowCount() {
        return petentList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Petent petent = petentList.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return petent.getPesel();
            case 1:
                return petent.getImie();
            case 2:
                return petent.getNazwisko();
            case 3:
                return petent.getAdres();
            case 4:
                return petent.getKodPocztowy();
            case 5:
                return petent.getTelefon();
            case 6:
                return petent.getEmail();
            case 7:
                return petent.getPlec().getPlec();
            case 8:
                return petent.getUwagi();
        }

        return petent;
    }

    public PetentControl getPetentControl() {
        return petentControl;
    }

    public void setPetentControl(PetentControl petentControl) {
        this.petentControl = petentControl;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }
}
