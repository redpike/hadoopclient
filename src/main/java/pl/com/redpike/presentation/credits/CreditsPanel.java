package pl.com.redpike.presentation.credits;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Redpike.
 */
public class CreditsPanel extends JPanel {

    private GridLayout layout;
    private GridBagConstraints gbc;
    private BufferedImage image;
    private JLabel picLabel;
    private JLabel headerLabel;

    public CreditsPanel() {
        init();
    }

    public void init() {
        initComponents();
        initLayout();
    }

    private void initComponents() {
        layout = new GridLayout(5, 1);
        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.VERTICAL;

        try {
            image = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\logoBig.png"));
            image.getScaledInstance(400, 240, Image.SCALE_SMOOTH);
        } catch (IOException e) {
            e.printStackTrace();
        }

        picLabel = new JLabel(new ImageIcon(image));
        headerLabel = new JLabel("Hadoop Client Application");
        headerLabel.setFont(new Font(headerLabel.getFont().getName(), Font.CENTER_BASELINE, 20));
    }

    private void initLayout() {
        setLayout(layout);
        setBackground(new Color(130, 210, 151));

        gbc.gridx = 0;
        gbc.gridy = 0;
        add(picLabel, gbc);

        gbc.gridy = 1;
        add(headerLabel, gbc);
    }
}
