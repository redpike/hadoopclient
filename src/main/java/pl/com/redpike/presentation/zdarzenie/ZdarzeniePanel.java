package pl.com.redpike.presentation.zdarzenie;

import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import pl.com.redpike.business.zdarzenie.Zdarzenie;
import pl.com.redpike.control.zdarzenie.ZdarzenieControl;
import pl.com.redpike.util.ButtonLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Redpike.
 */
public class ZdarzeniePanel extends JPanel {

    private GridBagLayout layout;
    private GridBagConstraints gbc;
    private ButtonLayout buttonLayout;

    private JLabel tableName;
    private WebTable table;
    private ZdarzenieTable zdarzenieTable;
    private WebScrollPane scrollPane;

    private Zdarzenie selectedZdarzenie;
    private ZdarzenieControl zdarzenieControl;

    public ZdarzeniePanel() {
        init();
    }

    private void init() {
        initComponents();
        initLayout();
        initListeners();
    }

    private void initComponents() {
        layout = new GridBagLayout();

        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.VERTICAL;

        tableName = new JLabel("Lista zdarzeń ");
        tableName.setFont(new Font(tableName.getFont().getName(), Font.ITALIC, 18));

        zdarzenieTable = new ZdarzenieTable();
        table = new WebTable(zdarzenieTable);
        table.setAutoResizeMode(WebTable.AUTO_RESIZE_OFF);
        table.setPreferredScrollableViewportSize(new Dimension(550, 290));
        scrollPane = new WebScrollPane(table);

        buttonLayout = new ButtonLayout();
    }

    private void initLayout() {
        setBackground(new Color(130, 210, 151));
        setLayout(layout);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 0, 10, 0);
        add(tableName, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        add(scrollPane, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(10, 0, 0, 0);
        add(buttonLayout, gbc);
    }

    private void initListeners() {
        table.getSelectionModel().addListSelectionListener(e -> {

        });

        getButtonLayout().getAddButton().addActionListener(e -> {

        });

        getButtonLayout().getEditButton().addActionListener(e -> {

        });

        getButtonLayout().getDeleteButton().addActionListener(e -> {

        });
    }

    public ButtonLayout getButtonLayout() {
        return buttonLayout;
    }
}
