package pl.com.redpike.presentation.zdarzenie;

import pl.com.redpike.business.zdarzenie.Zdarzenie;
import pl.com.redpike.control.zdarzenie.ZdarzenieControl;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by Redpike.
 */
public class ZdarzenieTable extends AbstractTableModel {

    private ZdarzenieControl zdarzenieControl = new ZdarzenieControl();
    private List<Zdarzenie> zdarzenieList;

    private String[] columnNames = {
            "ID",
            "Status",
            "Opening date",
            "Closing date",
            "Description",
            "Notes",
            "Id of employer"
    };

    public ZdarzenieTable() {
        zdarzenieList = zdarzenieControl.getAllZdarzenieFromDB();
    }

    @Override
    public int getRowCount() {
        return zdarzenieList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Zdarzenie zdarzenie = zdarzenieList.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return zdarzenie.getIdZdarzenia();
            case 1:
                return zdarzenie.getStatus().getNazwa();
            case 2:
                return zdarzenie.getDataRozpoczecia().toString();
            case 3:
                return zdarzenie.getDataZakonczenia().toString();
            case 4:
                return zdarzenie.getOpis();
            case 5:
                return zdarzenie.getUwagi();
            case 6:
                return zdarzenie.getIdPracownika();
        }

        return zdarzenie;
    }
}
