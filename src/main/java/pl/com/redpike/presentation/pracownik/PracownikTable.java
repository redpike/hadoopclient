package pl.com.redpike.presentation.pracownik;

import pl.com.redpike.business.pracownik.Pracownik;
import pl.com.redpike.control.pracownik.PracownikControl;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by Redpike.
 */
public class PracownikTable extends AbstractTableModel {

    private PracownikControl pracownikControl = new PracownikControl();
    private List<Pracownik> pracownikList;

    private String[] columnNames = {
            "ID",
            "Name",
            "Surname",
            "Address",
            "Postal code",
            "Phone Number",
            "Email address",
            "Sex",
            "Account number"
    };

    public PracownikTable() {
        pracownikList = pracownikControl.getAllPracownikFromDB();
    }

    @Override
    public int getRowCount() {
        return pracownikList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Pracownik pracownik = pracownikList.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return pracownik.getPesel();
            case 1:
                return pracownik.getImie();
            case 2:
                return pracownik.getNazwisko();
            case 3:
                return pracownik.getAdres();
            case 4:
                return pracownik.getKodPocztowy();
            case 5:
                return pracownik.getTelefon();
            case 6:
                return pracownik.getEmail();
            case 7:
                return pracownik.getPlec().getPlec();
            case 8:
                return pracownik.getNrKonta();
        }

        return pracownik;
    }
}
