package pl.com.redpike.presentation.komisariat;

import pl.com.redpike.business.komisariat.Komisariat;
import pl.com.redpike.control.komisariat.KomisariatControl;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by Redpike.
 */
public class KomisariatTable extends AbstractTableModel {

    private KomisariatControl komisariatControl = new KomisariatControl();
    private List<Komisariat> komisariatList;

    private String[] columnNames = {
            "Name",
            "Address",
            "Phone number",
            "Email address",
    };

    public KomisariatTable() {
        komisariatList = komisariatControl.getAllKomisariatFromDB();
    }

    @Override
    public int getRowCount() {
        return komisariatList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Komisariat komisariat = komisariatList.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return komisariat.getNazwa();
            case 1:
                return komisariat.getAdres();
            case 2:
                return komisariat.getTelefon();
            case 3:
                return komisariat.getEmail();
        }

        return komisariat;
    }

    public KomisariatControl getKomisariatControl() {
        return komisariatControl;
    }

    public void setKomisariatControl(KomisariatControl komisariatControl) {
        this.komisariatControl = komisariatControl;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }
}
