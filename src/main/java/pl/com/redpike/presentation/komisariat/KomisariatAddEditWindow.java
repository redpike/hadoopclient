package pl.com.redpike.presentation.komisariat;

import com.alee.laf.button.WebButton;
import com.alee.laf.text.WebTextField;
import pl.com.redpike.business.komisariat.Komisariat;
import pl.com.redpike.control.komisariat.KomisariatControl;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Redpike.
 */
public class KomisariatAddEditWindow extends JFrame {

    private JPanel mainPanel;
    private GridBagLayout layout;
    private GridBagConstraints gbc;

    private JLabel nameLabel;
    private JLabel addressLabel;
    private JLabel phoneLabel;
    private JLabel emailLabel;
    private WebTextField nameField;
    private WebTextField addressField;
    private WebTextField phoneField;
    private WebTextField emailField;

    private WebButton acceptButton;
    private WebButton cancelButton;

    private BufferedImage acceptIcon;
    private BufferedImage cancelIcon;

    private KomisariatControl komisariatControl;

    public KomisariatAddEditWindow(Komisariat komisariat) {
        init(komisariat);
    }

    private void init(Komisariat komisariat) {
        prepareWindow();
        centerWindow();
        initComponents(komisariat);
        initLayout();
        initListeners(komisariat);
    }

    private void prepareWindow() {
        setSize(250, 300);
        setTitle("Dodaj/Edytuj wpis");
        Image image = Toolkit.getDefaultToolkit()
                .createImage("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\logoSmall.png");
        setIconImage(image);
        setResizable(false);
        setVisible(true);
    }

    private void initComponents(Komisariat komisariat) {
        layout = new GridBagLayout();
        gbc = new GridBagConstraints();

        mainPanel = new JPanel();
        mainPanel.setLayout(layout);
        mainPanel.setBackground(new Color(130, 210, 151));

        nameLabel = new JLabel("Name");
        addressLabel = new JLabel("Address");
        phoneLabel = new JLabel("Phone");
        emailLabel = new JLabel("Email");

        if (komisariat == null) {
            nameField = new WebTextField(15);
            addressField = new WebTextField(15);
            phoneField = new WebTextField(15);
            emailField = new WebTextField(15);

        } else {
            nameField = new WebTextField(komisariat.getNazwa(), 15);
            addressField = new WebTextField(komisariat.getAdres(), 15);
            phoneField = new WebTextField(komisariat.getTelefon(), 15);
            emailField = new WebTextField(komisariat.getEmail(), 15);
        }

        initIcons();
        acceptButton = new WebButton("Accept", new ImageIcon(acceptIcon));
        cancelButton = new WebButton("Cancel", new ImageIcon(cancelIcon));

        komisariatControl = new KomisariatControl();
    }

    private void initLayout() {
        gbc.fill = GridBagConstraints.VERTICAL;

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(nameLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(nameField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(addressLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(addressField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(phoneLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(phoneField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(emailLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(emailField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.FIRST_LINE_START;
        gbc.insets = new Insets(30, 0, 0, 0);
        mainPanel.add(acceptButton, gbc);

        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.fill = GridBagConstraints.LAST_LINE_END;
        gbc.insets = new Insets(30, 90, 0, 0);
        mainPanel.add(cancelButton, gbc);

        add(mainPanel);
    }

    private void initListeners(Komisariat komisariat) {
        if (komisariat == null) {
            acceptButton.addActionListener(e -> {
                final Komisariat addedKomisariat = new Komisariat(komisariatControl.getMaxRow(), nameField.getText(), addressField.getText(), phoneField.getText(), emailField.getText());
                komisariatControl.addNewKomisariat(addedKomisariat);
                dispose();
            });
        } else {
            acceptButton.addActionListener(e -> {
                final Komisariat editedKomisariat = new Komisariat(komisariat.getId(), nameField.getText(), addressField.getText(), phoneField.getText(), emailField.getText());
                komisariatControl.editKomisariat(editedKomisariat);
                dispose();
                JOptionPane.showMessageDialog(null, "Record has been updated!");
            });
        }

        cancelButton.addActionListener(e ->
                dispose()
        );
    }

    private void centerWindow() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - getHeight()) / 2);
        setLocation(x, y);
    }

    private void initIcons() {
        try {
            acceptIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\acceptImg.png"));

            cancelIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\cancelImg.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
