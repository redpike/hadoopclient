package pl.com.redpike.presentation.komisariat;

import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.table.WebTable;
import pl.com.redpike.business.komisariat.Komisariat;
import pl.com.redpike.control.komisariat.KomisariatControl;
import pl.com.redpike.util.ButtonLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Redpike.
 */
public class KomisariatPanel extends JPanel {

    private GridBagLayout layout;
    private GridBagConstraints gbc;
    private ButtonLayout buttonLayout;

    private JLabel tableName;
    private WebTable table;
    private KomisariatTable komisariatTable;
    private WebScrollPane scrollPane;

    private Komisariat selectedKomisariat;
    private KomisariatControl komisariatControl;

    public KomisariatPanel() {
        init();
    }

    private void init() {
        initComponents();
        initLayout();
        initListeners();
    }

    private void initComponents() {
        layout = new GridBagLayout();

        gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.VERTICAL;

        tableName = new JLabel("Lista komisariatów ");
        tableName.setFont(new Font(tableName.getFont().getName(), Font.ITALIC, 18));

        komisariatTable = new KomisariatTable();
        table = new WebTable(komisariatTable);
        table.setPreferredScrollableViewportSize(new Dimension(550, 290));
        scrollPane = new WebScrollPane(table);

        buttonLayout = new ButtonLayout();
    }

    private void initLayout() {
        setBackground(new Color(130, 210, 151));
        setLayout(layout);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 0, 10, 0);
        add(tableName, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        add(scrollPane, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(10, 0, 0, 0);
        add(buttonLayout, gbc);
    }

    private void initListeners() {
        table.getSelectionModel().addListSelectionListener(e -> {
            selectedKomisariat = new Komisariat();
            selectedKomisariat.setId((long) table.getSelectedRow());
            selectedKomisariat.setNazwa((String) komisariatTable.getValueAt(table.getSelectedRow(), 0));
            selectedKomisariat.setAdres((String) komisariatTable.getValueAt(table.getSelectedRow(), 1));
            selectedKomisariat.setTelefon((String) komisariatTable.getValueAt(table.getSelectedRow(), 2));
            selectedKomisariat.setEmail((String) komisariatTable.getValueAt(table.getSelectedRow(), 3));
        });

        getButtonLayout().getAddButton().addActionListener(e -> {
            new KomisariatAddEditWindow(null);
        });

        getButtonLayout().getEditButton().addActionListener(e -> {
            new KomisariatAddEditWindow(selectedKomisariat);
        });

        getButtonLayout().getDeleteButton().addActionListener(e -> {
            komisariatControl = new KomisariatControl();
            komisariatControl.deleteKomisariat(selectedKomisariat);
            JOptionPane.showMessageDialog(null, "Selected record has been deleted!");
            komisariatTable.fireTableDataChanged();
        });
    }

    public ButtonLayout getButtonLayout() {
        return buttonLayout;
    }
}
