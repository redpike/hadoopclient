package pl.com.redpike.presentation.login;

import com.alee.laf.button.WebButton;
import com.alee.laf.text.WebPasswordField;
import com.alee.laf.text.WebTextField;
import com.alee.managers.notification.NotificationManager;
import pl.com.redpike.business.uzytkownik.Uzytkownik;
import pl.com.redpike.control.login.UserAuthorization;
import pl.com.redpike.presentation.main.MainFrame;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Redpike.
 */
public class LoginFrame extends JFrame {

    private GridBagLayout mainLayout;
    private GridBagConstraints bagConstraints;
    private JPanel panel;
    private JLabel logoLabel;
    private BufferedImage logo;
    private WebTextField loginField;
    private WebPasswordField passwordField;
    private WebButton loginButton;
    private BufferedImage loginIcon;
    private MainFrame mainFrame;

    public void init() {
        prepareWindow();
        initComponents();
        initLayout();
        initListeners();
        setVisible(true);
    }

    private void prepareWindow() {
        setSize(200, 300);
        centerWindow();
        setTitle("Kronika");
        Image image = Toolkit.getDefaultToolkit()
                .createImage("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\logoSmall.png");
        setIconImage(image);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void centerWindow() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - getHeight()) / 2);
        setLocation(x, y);
    }

    private void initComponents() {
        panel = new JPanel();
        panel.setBackground(new Color(130, 210, 151));

        try {
            logo = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\logoSmall.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Image resizedIcon = logo.getScaledInstance(100, 100, Image.SCALE_FAST);
        logoLabel = new JLabel(new ImageIcon(resizedIcon));
        logoLabel.repaint();

        bagConstraints = new GridBagConstraints();

        loginField = new WebTextField(20);
        loginField.setInputPrompt("Enter login...");

        passwordField = new WebPasswordField(20);
        passwordField.setInputPrompt("Enter password...");

        try {
            loginIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\login.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        loginButton = new WebButton("Login", new ImageIcon(loginIcon));

        getRootPane().setDefaultButton(loginButton);

        mainLayout = new GridBagLayout();
    }

    private void initLayout() {
        panel.setLayout(mainLayout);

        bagConstraints.fill = GridBagConstraints.VERTICAL;
        bagConstraints.insets = new Insets(0, 0, 25, 0);

        bagConstraints.gridx = 0;
        bagConstraints.gridy = 0;
        panel.add(logoLabel, bagConstraints);

        bagConstraints.gridx = 0;
        bagConstraints.gridy = 1;
        bagConstraints.ipadx = 130;
        bagConstraints.insets = new Insets(0, 0, 2, 0);
        panel.add(loginField, bagConstraints);

        bagConstraints.gridx = 0;
        bagConstraints.gridy = 3;
        bagConstraints.ipadx = 130;
        panel.add(passwordField, bagConstraints);

        bagConstraints.gridx = 0;
        bagConstraints.gridy = 6;
        bagConstraints.ipadx = 40;
        bagConstraints.insets = new Insets(5, 0, 0, 0);
        panel.add(loginButton, bagConstraints);

        add(panel);
    }

    private void initListeners() {
        loginButton.addActionListener(e -> {
            if (UserAuthorization.isInDatabase(loginField.getText(), passwordField.getText())) {
                Uzytkownik uzytkownik = new Uzytkownik(loginField.getText(), passwordField.getText());
                dispose();
                mainFrame = new MainFrame(uzytkownik);
                mainFrame.init();
                NotificationManager.showNotification("Welcome " + uzytkownik.getUsername());
            } else {
                JOptionPane.showMessageDialog(null, "Incorect username or password. Try again...");
            }
        });
    }
}
