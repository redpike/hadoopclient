package pl.com.redpike.business.petent;

import pl.com.redpike.business.enums.Plec;
import pl.com.redpike.control.hive.HiveConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Redpike.
 */
public class PetentDAO implements PetentLocal {

    private ResultSet resultSet;

    @Override
    public List<Petent> getAllPetent() {
        Statement statement;

        try {
            statement = HiveConnector.connectWithHive().createStatement();
            List<Petent> petentList = new ArrayList<>();
            resultSet = statement != null ? statement.executeQuery("SELECT * FROM petenci") : null;

            while (resultSet != null && resultSet.next()) {
                Petent petent = createPetentObject(resultSet);
                petentList.add(petent);
            }

            resultSet.close();
            statement.close();

            return petentList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Petent createPetentObject(ResultSet resultSet) {
        Petent petent = new Petent();
        try {
            petent.setPesel(resultSet.getString(1));
            petent.setImie(resultSet.getString(2));
            petent.setNazwisko(resultSet.getString(3));
            petent.setAdres(resultSet.getString(4));
            petent.setKodPocztowy(resultSet.getString(5));
            petent.setTelefon(resultSet.getString(6));
            petent.setEmail(resultSet.getString(7));

            if (resultSet.getString(8).equalsIgnoreCase(Plec.K.getDbValue()))
                petent.setPlec(Plec.K);
            else
                petent.setPlec(Plec.M);

            petent.setUwagi(resultSet.getString(9));

            return petent;
        } catch (SQLException e) {
            e.printStackTrace();
            return petent;
        }
    }
}
