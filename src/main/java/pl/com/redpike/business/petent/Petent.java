package pl.com.redpike.business.petent;

import pl.com.redpike.business.enums.Plec;

/**
 * Created by Redpike.
 */
public class Petent {

    private String pesel;
    private String imie;
    private String nazwisko;
    private String adres;
    private String kodPocztowy;
    private String telefon;
    private String email;
    private Plec plec;
    private String uwagi;

    public Petent() {
    }

    public Petent(String pesel, String imie, String nazwisko, String adres, String kodPocztowy, String telefon, String email, Plec plec, String uwagi) {
        this.pesel = pesel;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
        this.kodPocztowy = kodPocztowy;
        this.telefon = telefon;
        this.email = email;
        this.plec = plec;
        this.uwagi = uwagi;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getKodPocztowy() {
        return kodPocztowy;
    }

    public void setKodPocztowy(String kodPocztowy) {
        this.kodPocztowy = kodPocztowy;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Plec getPlec() {
        return plec;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public String getUwagi() {
        return uwagi;
    }

    public void setUwagi(String uwagi) {
        this.uwagi = uwagi;
    }

    @Override
    public String toString() {
        return "Petent{" +
                "pesel='" + pesel + '\'' +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", adres='" + adres + '\'' +
                ", kodPocztowy='" + kodPocztowy + '\'' +
                ", telefon='" + telefon + '\'' +
                ", email='" + email + '\'' +
                ", plec=" + plec +
                ", uwagi='" + uwagi + '\'' +
                '}';
    }
}
