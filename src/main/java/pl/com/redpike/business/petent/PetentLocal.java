package pl.com.redpike.business.petent;

import java.util.List;

/**
 * Created by Redpike.
 */
public interface PetentLocal {

    public List<Petent> getAllPetent();
}
