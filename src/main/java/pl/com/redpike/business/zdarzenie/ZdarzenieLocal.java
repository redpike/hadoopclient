package pl.com.redpike.business.zdarzenie;

import java.util.List;

/**
 * Created by Redpike.
 */
public interface ZdarzenieLocal {

    public List<Zdarzenie> getAllZdarzenie();
}
