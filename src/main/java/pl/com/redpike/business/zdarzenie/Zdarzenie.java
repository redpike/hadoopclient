package pl.com.redpike.business.zdarzenie;

import pl.com.redpike.business.enums.Sprawa;
import pl.com.redpike.business.enums.Status;

import java.util.Date;

/**
 * Created by Redpike.
 */
public class Zdarzenie {

    private int idZdarzenia;
    private Status status;
    private Sprawa sprawa;
    private Date dataRozpoczecia;
    private Date dataZakonczenia;
    private String opis;
    private String uwagi;
    private String idPracownika;

    public Zdarzenie() {
    }

    public Zdarzenie(int idZdarzenia, Status status, Sprawa sprawa, Date dataRozpoczecia, Date dataZakonczenia, String opis, String uwagi, String idPracownika) {
        this.idZdarzenia = idZdarzenia;
        this.status = status;
        this.sprawa = sprawa;
        this.dataRozpoczecia = dataRozpoczecia;
        this.dataZakonczenia = dataZakonczenia;
        this.opis = opis;
        this.uwagi = uwagi;
        this.idPracownika = idPracownika;
    }

    public int getIdZdarzenia() {
        return idZdarzenia;
    }

    public void setIdZdarzenia(int idZdarzenia) {
        this.idZdarzenia = idZdarzenia;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Sprawa getSprawa() {
        return sprawa;
    }

    public void setSprawa(Sprawa sprawa) {
        this.sprawa = sprawa;
    }

    public Date getDataRozpoczecia() {
        return dataRozpoczecia;
    }

    public void setDataRozpoczecia(Date dataRozpoczecia) {
        this.dataRozpoczecia = dataRozpoczecia;
    }

    public Date getDataZakonczenia() {
        return dataZakonczenia;
    }

    public void setDataZakonczenia(Date dataZakonczenia) {
        this.dataZakonczenia = dataZakonczenia;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getUwagi() {
        return uwagi;
    }

    public void setUwagi(String uwagi) {
        this.uwagi = uwagi;
    }

    public String getIdPracownika() {
        return idPracownika;
    }

    public void setIdPracownika(String idPracownika) {
        this.idPracownika = idPracownika;
    }

    @Override
    public String toString() {
        return "Zdarzenie{" +
                "idZdarzenia=" + idZdarzenia +
                ", status=" + status +
                ", sprawa=" + sprawa +
                ", dataRozpoczecia=" + dataRozpoczecia +
                ", dataZakonczenia=" + dataZakonczenia +
                ", opis='" + opis + '\'' +
                ", uwagi='" + uwagi + '\'' +
                ", idPracownika='" + idPracownika + '\'' +
                '}';
    }
}
