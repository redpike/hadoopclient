package pl.com.redpike.business.zdarzenie;

import pl.com.redpike.business.enums.Status;
import pl.com.redpike.control.hive.HiveConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Redpike.
 */
public class ZdarzenieDAO implements ZdarzenieLocal {

    private ResultSet resultSet;

    @Override
    public List<Zdarzenie> getAllZdarzenie() {
        Statement statement;

        try {
            statement = HiveConnector.connectWithHive().createStatement();
            List<Zdarzenie> zdarzenieList = new ArrayList<>();
            resultSet = statement != null ? statement.executeQuery("SELECT * FROM zdarzenia") : null;

            while (resultSet != null && resultSet.next()) {
                Zdarzenie zdarzenie = createZdarzenieObject(resultSet);
                zdarzenieList.add(zdarzenie);
            }

            resultSet.close();
            statement.close();

            return zdarzenieList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Zdarzenie createZdarzenieObject(ResultSet resultSet) {
        Zdarzenie zdarzenie = new Zdarzenie();
        try {
            zdarzenie.setIdZdarzenia(resultSet.getInt(1));

            if (resultSet.getInt(2) == Status.OTWARTY.getDbValue())
                zdarzenie.setStatus(Status.OTWARTY);
            else if (resultSet.getInt(2) == Status.WERYFIKOWANY.getDbValue())
                zdarzenie.setStatus(Status.WERYFIKOWANY);
            else if (resultSet.getInt(2) == Status.PRZYJETY.getDbValue())
                zdarzenie.setStatus(Status.PRZYJETY);
            else if (resultSet.getInt(2) == Status.W_TRAKCIE_REALIZACJI.getDbValue())
                zdarzenie.setStatus(Status.W_TRAKCIE_REALIZACJI);
            else if (resultSet.getInt(2) == Status.ZAKONCZONY.getDbValue())
                zdarzenie.setStatus(Status.ZAKONCZONY);
            else
                zdarzenie.setStatus(Status.ANULOWANY);

            zdarzenie.setDataRozpoczecia(resultSet.getDate(4));
            zdarzenie.setDataZakonczenia(resultSet.getDate(5));
            zdarzenie.setOpis(resultSet.getString(6));
            zdarzenie.setUwagi(resultSet.getString(7));
            zdarzenie.setIdPracownika(resultSet.getString(8));

            return zdarzenie;
        } catch (SQLException e) {
            e.printStackTrace();
            return zdarzenie;
        }
    }
}
