package pl.com.redpike.business.pracownik;

import pl.com.redpike.business.enums.Plec;
import pl.com.redpike.control.hive.HiveConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Redpike.
 */
public class PracownikDAO implements PracownikLocal {

    private ResultSet resultSet;

    @Override
    public List<Pracownik> getAllPracownik() {
        Statement statement;

        try {
            statement = HiveConnector.connectWithHive().createStatement();
            List<Pracownik> pracownikList = new ArrayList<>();
            resultSet = statement != null ? statement.executeQuery("SELECT * FROM pracownicy") : null;

            while (resultSet != null && resultSet.next()) {
                Pracownik pracownik = createPracownikObject(resultSet);
                pracownikList.add(pracownik);
            }

            resultSet.close();
            statement.close();

            return pracownikList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Pracownik createPracownikObject(ResultSet resultSet) {
        Pracownik pracownik = new Pracownik();
        try {
            pracownik.setPesel(resultSet.getString(1));
            pracownik.setImie(resultSet.getString(2));
            pracownik.setNazwisko(resultSet.getString(3));
            pracownik.setAdres(resultSet.getString(4));
            pracownik.setKodPocztowy(resultSet.getString(5));
            pracownik.setTelefon(resultSet.getString(6));
            pracownik.setEmail(resultSet.getString(7));

            if (resultSet.getString(8).equalsIgnoreCase(Plec.K.getDbValue()))
                pracownik.setPlec(Plec.K);
            else
                pracownik.setPlec(Plec.M);

            pracownik.setNrKonta(resultSet.getString(9));

            return pracownik;
        } catch (SQLException e) {
            e.printStackTrace();
            return pracownik;
        }
    }
}
