package pl.com.redpike.business.pracownik;

import java.util.List;

/**
 * Created by Redpike.
 */
public interface PracownikLocal {

    public List<Pracownik> getAllPracownik();
}
