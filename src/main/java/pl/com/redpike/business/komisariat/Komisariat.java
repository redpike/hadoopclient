package pl.com.redpike.business.komisariat;

/**
 * Created by Redpike.
 */
public class Komisariat {

    private Long id;
    private String nazwa;
    private String adres;
    private String telefon;
    private String email;

    public Komisariat() {
    }

    public Komisariat(Long id, String nazwa, String adres, String telefon, String email) {
        this.id = id;
        this.nazwa = nazwa;
        this.adres = adres;
        this.telefon = telefon;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Komisariat{" +
                "id=" + id +
                ", nazwa='" + nazwa + '\'' +
                ", adres='" + adres + '\'' +
                ", telefon='" + telefon + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
