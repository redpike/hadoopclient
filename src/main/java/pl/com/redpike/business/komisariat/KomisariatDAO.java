package pl.com.redpike.business.komisariat;

import pl.com.redpike.control.hive.HiveConnector;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Redpike.
 */
public class KomisariatDAO implements KomisariatLocal {

    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    @Override
    public Komisariat getKomisariat(Long komisariatId) {
        try {
            preparedStatement = HiveConnector.connectWithHive().prepareStatement("select * from komisariat k where k.id = ?");
            preparedStatement.setLong(1, komisariatId);
            resultSet = preparedStatement.executeQuery();
            Komisariat komisariat = null;

            while (resultSet != null && resultSet.next()) {
                komisariat = createKomisariatObject(resultSet);
            }

            resultSet.close();
            preparedStatement.close();

            return komisariat;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Komisariat> getAllKomisariat() {
        Statement statement;

        try {
            statement = HiveConnector.connectWithHive().createStatement();
            List<Komisariat> komisariatList = new ArrayList<>();
            resultSet = statement != null ? statement.executeQuery("SELECT * FROM komisariat") : null;

            while (resultSet != null && resultSet.next()) {
                Komisariat komisariat = createKomisariatObject(resultSet);
                komisariatList.add(komisariat);
            }

            resultSet.close();
            statement.close();

            return komisariatList;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void addKomisariat(Komisariat komisariat) {
        try {
            preparedStatement = HiveConnector.connectWithHive().prepareStatement("INSERT INTO TABLE komisariat VALUES(?, ?, ?, ?, ?)");
            preparedStatement.setLong(1, komisariat.getId());
            preparedStatement.setString(2, komisariat.getNazwa());
            preparedStatement.setString(3, komisariat.getAdres());
            preparedStatement.setString(4, komisariat.getTelefon());
            preparedStatement.setString(5, komisariat.getEmail());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void editKomisariat(Komisariat komisariat) {
        try {
            preparedStatement = HiveConnector.connectWithHive().prepareStatement("UPDATE komisariat SET nazwa = ?, adres = ?, telefon = ?, email = ? WHERE id_komendy = ?");
            preparedStatement.setString(1, komisariat.getNazwa());
            preparedStatement.setString(2, komisariat.getAdres());
            preparedStatement.setString(3, komisariat.getTelefon());
            preparedStatement.setString(4, komisariat.getEmail());
            preparedStatement.setLong(5, komisariat.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeKomisariat(Komisariat komisariat) {
        try {
            preparedStatement = HiveConnector.connectWithHive().prepareStatement("DELETE FROM komisariat WHERE id_komendy = ?");
            preparedStatement.setLong(1, komisariat.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Komisariat createKomisariatObject(ResultSet resultSet) {
        Komisariat komisariat = new Komisariat();
        try {
            komisariat.setId(resultSet.getLong(1));
            komisariat.setNazwa(resultSet.getString(2));
            komisariat.setAdres(resultSet.getString(3));
            komisariat.setTelefon(resultSet.getString(4));
            komisariat.setEmail(resultSet.getString(5));

            return komisariat;
        } catch (SQLException e) {
            e.printStackTrace();
            return komisariat;
        }
    }
}
