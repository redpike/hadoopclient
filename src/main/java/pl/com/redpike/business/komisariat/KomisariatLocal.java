package pl.com.redpike.business.komisariat;

import java.util.List;

/**
 * Created by Redpike.
 */
public interface KomisariatLocal {

    public Komisariat getKomisariat(Long komisariatId);

    public List<Komisariat> getAllKomisariat();

    public void addKomisariat(Komisariat komisariat);

    public void editKomisariat(Komisariat komisariat);

    public void removeKomisariat(Komisariat komisariat);
}
