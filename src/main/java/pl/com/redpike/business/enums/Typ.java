package pl.com.redpike.business.enums;

/**
 * Created by Redpike.
 */
public enum Typ {

    PETENT(1, "Petent"),
    POSZKODOWANY(2, "Poszkodowany"),
    SPRAWCA(3, "Sprawca"),
    SWIADEK(4, "Swiadek"),
    SWIADEK_KORONNY(5, "Swiadek koronny"),
    INNE(6, "Inne");

    private int dbValue;
    private String nazwa;

    Typ(int dbValue, String nazwa) {
        this.dbValue = dbValue;
        this.nazwa = nazwa;
    }

    public int getDbValue() {
        return dbValue;
    }

    public void setDbValue(int dbValue) {
        this.dbValue = dbValue;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
