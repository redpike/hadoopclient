package pl.com.redpike.business.enums;

/**
 * Created by Redpike.
 */
public enum Status {

    OTWARTY(1, "Otwarty"),
    WERYFIKOWANY(2, "Weryfikowany"),
    PRZYJETY(3, "Przyjety"),
    W_TRAKCIE_REALIZACJI(4, "W trakcie realizacji"),
    ZAKONCZONY(5, "Zakonczony"),
    ANULOWANY(6, "Anulowanu");

    private int dbValue;
    private String nazwa;

    Status(int dbValue, String nazwa) {
        this.dbValue = dbValue;
        this.nazwa = nazwa;
    }

    public int getDbValue() {
        return dbValue;
    }

    public void setDbValue(int dbValue) {
        this.dbValue = dbValue;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
