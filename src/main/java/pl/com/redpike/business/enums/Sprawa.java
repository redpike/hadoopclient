package pl.com.redpike.business.enums;

/**
 * Created by Redpike.
 */
public enum Sprawa {

    ZACH_ANT(1, "zachowanie antyspoleczne"),
    RABUN(2, "rabunek z uzyciem broni"),
    PODPAL(3, "podpalenie"),
    NAPASC(4, "napasc"),
    SZANTAZ(5, "szantaz"),
    WLAMANIE(6, "wlamanie"),
    UPROWADZENIE(7, "uprowadzenie [np. samolotu]"),
    PRZECH_PRZEZ_JEZD(8, "nieprawidlowe przechodzenie przez jezdnie"),
    PORWANIE(9, "porwanie [czlowieka]"),
    ROZBOJ(10, "rozboj [na ulicy]"),
    MORDERSTWO(11, "morderstwo"),
    KRADZIEZ_KIESZ(12, "kradziez kieszonkowa"),
    GWALT(13, "gwalt"),
    RABUNEK(14, "rabunek"),
    KRADZIEZ_SKLEP(15, "kradziez sklepowa"),
    PRZEMYT(16, "przemyt"),
    KRADZIEZ(17, "kradziez"),
    PRZEMOC(18, "przemoc"),
    INNE(19, "inne");

    private int dbValue;
    private String nazwa;

    Sprawa(int dbValue, String nazwa) {
        this.dbValue = dbValue;
        this.nazwa = nazwa;
    }

    public int getDbValue() {
        return dbValue;
    }

    public void setDbValue(int dbValue) {
        this.dbValue = dbValue;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
