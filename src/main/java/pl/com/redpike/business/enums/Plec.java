package pl.com.redpike.business.enums;

/**
 * Created by Redpike.
 */
public enum Plec {

    K("K", "Women"),
    M("M", "Men");

    private String dbValue;
    private String plec;

    Plec(String dbValue, String plec) {
        this.dbValue = dbValue;
        this.plec = plec;
    }

    public String getDbValue() {
        return dbValue;
    }

    public void setDbValue(String dbValue) {
        this.dbValue = dbValue;
    }

    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }
}
