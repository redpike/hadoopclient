package pl.com.redpike.business.enums;

/**
 * Created by Redpike.
 */
public enum Ranga {

    POST(1, "Posterunkowy"),
    ST_POST(2, "Starszy Posterunkowy"),
    SIER(3, "Sierzant"),
    ST_SIER(4, "Starszy sierzant"),
    SIER_SZTAB(5, "Sierzant sztabowy"),
    MLOD_ASP(6, "Mlodszy aspirant"),
    ASP(7, "Aspirant"),
    STAR_ASP(8, "Starszy aspirant"),
    ASP_SZTAB(9, "Aspirant sztabowy"),
    PODKOM(10, "Podkomisarz"),
    KOM(11, "Komisarz"),
    NADKOM(12, "Nadkomisarz"),
    PODINSP(13, "Podinspektor"),
    MLOD_INSP(14, "Mlodszy inspektor"),
    INSP(15, "Inspektor"),
    NADINSP(16, "Nadinspektor"),
    GEN_INSP(17, "Generalny inspektor");

    private int dbValue;
    private String nazwa;

    Ranga(int dbValue, String nazwa) {
        this.dbValue = dbValue;
        this.nazwa = nazwa;
    }

    public int getDbValue() {
        return dbValue;
    }

    public void setDbValue(int dbValue) {
        this.dbValue = dbValue;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
