package pl.com.redpike.business.uzytkownik;

import pl.com.redpike.control.hive.HiveConnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Redpike.
 */
public class UzytkownikDAO implements UzytkownikLocal {

    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private Uzytkownik uzytkownik;

    @Override
    public Uzytkownik getUzytkownikFromDB(String username, String password) {
        Connection connection = HiveConnector.connectWithHive();

        try {
            preparedStatement = connection.prepareStatement("select username, password from uzytkownik where username = ? and password = ?");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                uzytkownik = new Uzytkownik();
                uzytkownik.setUsername(resultSet.getString(1));
                uzytkownik.setPassword(resultSet.getString(2));
            }

            return uzytkownik;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Uzytkownik getUzytkownik() {
        return uzytkownik;
    }

    public void setUzytkownik(Uzytkownik uzytkownik) {
        this.uzytkownik = uzytkownik;
    }
}
