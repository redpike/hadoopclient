package pl.com.redpike.business.uzytkownik;

/**
 * Created by Redpike.
 */
public interface UzytkownikLocal {

    public Uzytkownik getUzytkownikFromDB(String username, String password);
}
