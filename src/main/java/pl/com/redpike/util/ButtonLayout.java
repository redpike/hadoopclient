package pl.com.redpike.util;

import com.alee.laf.button.WebButton;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Redpike.
 */
public class ButtonLayout extends JPanel {

    private GridBagLayout buttonLayout;
    private GridBagConstraints gbc;
    private WebButton addButton;
    private WebButton editButton;
    private WebButton deleteButton;
    private BufferedImage addIcon;
    private BufferedImage editIcon;
    private BufferedImage deleteIcon;

    public ButtonLayout() {
        init();
    }

    private void init() {
        initComponents();
        initLayout();
    }

    private void initComponents() {
        loadIcons();

        buttonLayout = new GridBagLayout();
        gbc = new GridBagConstraints();

        addButton = new WebButton("Add", new ImageIcon(addIcon));
        editButton = new WebButton("Edit", new ImageIcon(editIcon));
        deleteButton = new WebButton("Delete", new ImageIcon(deleteIcon));
    }

    private void initLayout() {
        setLayout(buttonLayout);
        setBackground(new Color(130, 210, 151));

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.ipadx = 50;
        add(addButton, gbc);

        gbc.gridx = 1;
        gbc.insets = new Insets(0, 10, 0, 10);
        add(editButton, gbc);

        gbc.gridx = 2;
        gbc.insets = new Insets(0, 0, 0, 0);
        add(deleteButton, gbc);
    }

    private void loadIcons() {
        try {
            addIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\acceptImg.png"));

            editIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\addImg.png"));

            deleteIcon = ImageIO
                    .read(new File("D:\\Google Drive\\Rafal\\Uczelnia\\Magisterka\\Semestr I\\Zaawansowane Technologie Baz Danych\\Projekt\\HadoopClient\\src\\main\\resources\\icons\\delImg.png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public WebButton getAddButton() {
        return addButton;
    }

    public void setAddButton(WebButton addButton) {
        this.addButton = addButton;
    }

    public WebButton getEditButton() {
        return editButton;
    }

    public void setEditButton(WebButton editButton) {
        this.editButton = editButton;
    }

    public WebButton getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(WebButton deleteButton) {
        this.deleteButton = deleteButton;
    }
}
