package pl.com.redpike.control.zdarzenie;

import pl.com.redpike.business.zdarzenie.Zdarzenie;
import pl.com.redpike.business.zdarzenie.ZdarzenieDAO;

import java.util.List;

/**
 * Created by Redpike.
 */
public class ZdarzenieControl {

    private ZdarzenieDAO zdarzenieDAO;

    public ZdarzenieControl() {
        zdarzenieDAO = new ZdarzenieDAO();
    }

    public List<Zdarzenie> getAllZdarzenieFromDB() {
        return zdarzenieDAO.getAllZdarzenie();
    }
}
