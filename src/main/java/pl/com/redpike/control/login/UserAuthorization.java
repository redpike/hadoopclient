package pl.com.redpike.control.login;

import pl.com.redpike.business.uzytkownik.Uzytkownik;
import pl.com.redpike.business.uzytkownik.UzytkownikDAO;

/**
 * Created by Redpike.
 */
public class UserAuthorization {

    private static boolean isLogged = false;
    private static UzytkownikDAO uzytkownikDAO;

    public static boolean isInDatabase(String username, String password) {
        uzytkownikDAO = new UzytkownikDAO();

        if (uzytkownikDAO.getUzytkownikFromDB(username, password) != null)
            isLogged = true;

        return isLogged;
    }


    public static boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }

}
