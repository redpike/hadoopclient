package pl.com.redpike.control.komisariat;

import pl.com.redpike.business.komisariat.Komisariat;
import pl.com.redpike.business.komisariat.KomisariatDAO;

import java.util.List;

/**
 * Created by Redpike.
 */
public class KomisariatControl {

    private KomisariatDAO komisariatDAO;

    public KomisariatControl() {
        komisariatDAO = new KomisariatDAO();
    }

    public List<Komisariat> getAllKomisariatFromDB() {
        return komisariatDAO.getAllKomisariat();
    }

    public void addNewKomisariat(Komisariat komisariat) {
        komisariatDAO.addKomisariat(komisariat);
    }

    public void editKomisariat(Komisariat komisariat) {
        komisariatDAO.editKomisariat(komisariat);
    }

    public void deleteKomisariat(Komisariat komisariat) {
        komisariatDAO.removeKomisariat(komisariat);
    }

    public Long getMaxRow() {
        return (long) (komisariatDAO.getAllKomisariat().size() + 1);
    }
}
