package pl.com.redpike.control.pracownik;

import pl.com.redpike.business.pracownik.Pracownik;
import pl.com.redpike.business.pracownik.PracownikDAO;

import java.util.List;

/**
 * Created by Redpike.
 */
public class PracownikControl {

    private PracownikDAO pracownikDAO;

    public PracownikControl() {
        pracownikDAO = new PracownikDAO();
    }

    public List<Pracownik> getAllPracownikFromDB() {
        return pracownikDAO.getAllPracownik();
    }
}
