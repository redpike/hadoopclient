package pl.com.redpike.control.petent;

import pl.com.redpike.business.petent.Petent;
import pl.com.redpike.business.petent.PetentDAO;

import java.util.List;

/**
 * Created by Redpike.
 */
public class PetentControl {

    private PetentDAO petentDAO;

    public PetentControl() {
        petentDAO = new PetentDAO();
    }

    public List<Petent> getAllPetentFromDB() {
        return petentDAO.getAllPetent();
    }
}
