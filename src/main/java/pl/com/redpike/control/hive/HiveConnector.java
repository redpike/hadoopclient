package pl.com.redpike.control.hive;

import java.sql.*;

/**
 * Created by Redpike.
 */
public class HiveConnector {

    public static Connection connectWithHive() {
        try {
            return DriverManager.getConnection("jdbc:hive2://192.168.56.101:10000/default", "", "");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
